import java.io.*;
import java.util.ArrayList;

/**
 * Created by shumailovr on 9.12.14.
 */

public class CSVReader {

    ArrayList<String> stemmedWords = new ArrayList<String>();
    ArrayList<Float> valences = new ArrayList<Float>();

    public ArrayList<String> getStemmedWords() {
        return stemmedWords;
    }

    public void setStemmedWords(ArrayList<String> stemmedWords) {
        this.stemmedWords = stemmedWords;
    }

    public ArrayList<Float> getValences() {
        return valences;
    }

    public void setValences(ArrayList<Float> valences) {
        this.valences = valences;
    }

    /**
     * Takes full affective ratings csv file, reads it line by line, stems every word and saves it with
     * mean valence sum to new csv file
     * @param csvFilename
     * @param newCSVFilename
     */
    public void readStemAndWriteCSV(String csvFilename, String newCSVFilename) {

        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";
        Stemmer stemmer;

        try {
            br = new BufferedReader(new FileReader(csvFilename));

            while ((line = br.readLine()) != null) {
                // Read line from file
                String[] wordInfo = line.split(cvsSplitBy);
                String word = wordInfo[1];
                String valence = wordInfo[2];

//                System.out.println("word " + word
//                        + " , valence mean sum " + valence);

                //Stem the word
                stemmer = new Stemmer();
                String stemmedWord = stemmer.stemString(word);

//                System.out.println("stemmed word " + stemmedWord
//                        + " , valence mean sum " + valence);

                //Save stemmed word to new csv file with its valence
                writeCSV(newCSVFilename, stemmedWord, valence);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Done");
    }


    /**
     * Code to write one entry to existing csv file with comma separator
     * @param filename
     * @param word
     * @param valence
     */
    public void writeCSV(String filename, String word, String valence) {

        String newCSVFile = filename; //"affectiveRatings/Ratings_Warriner_et_al.csv"
        try {
            FileWriter writer = new FileWriter(newCSVFile, true);

            writer.append(word);
            writer.append(',');
            writer.append(valence);
            writer.append('\n');

            writer.flush();
            writer.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Reads word valences csv file
     * @param filename
     */
    public void readRatingsCSV(String filename) {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(filename));

            //This is first line that contains only name of the column and has to be ignored
            br.readLine();

            while ((line = br.readLine()) != null) {
                // Read line from file
                String[] wordInfo = line.split(cvsSplitBy);
                String word = wordInfo[0];
                String valence = wordInfo[1];

                //Since collection of rated words contains sometimes words with same
                //stem, I will just take the first word. In this work analysis of
                //which stem is better to take for valence is not the priority
                if (!stemmedWords.contains(word)) {
                    stemmedWords.add(word);
                    valences.add(Float.parseFloat(valence));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Done");
    }
}