import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by shumailovr on 30.01.15.
 */

@Embeddable
public class TrainingEuclideanDistanceId implements Serializable {

    //isbn == asin
    private String isbn1;
    private String isbn2;

    public TrainingEuclideanDistanceId() {
    }

    public TrainingEuclideanDistanceId(String isbn1, String isbn2) {
        this.isbn1 = isbn1;
        this.isbn2 = isbn2;
    }

    @Column(name = "isbn1")
    public String getIsbn1() {
        return isbn1;
    }

    public void setIsbn1(String isbn1) {
        this.isbn1 = isbn1;
    }

    @Column(name = "isbn2")
    public String getIsbn2() {
        return isbn2;
    }

    public void setIsbn2(String isbn2) {
        this.isbn2 = isbn2;
    }
}
