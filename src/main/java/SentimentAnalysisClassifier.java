import com.aliasi.classify.Classification;
import com.aliasi.classify.LMClassifier;
import com.aliasi.util.AbstractExternalizable;

import java.io.File;
import java.io.IOException;

/**
 * Created by zen on 11/16/14.
 */
public class SentimentAnalysisClassifier {

    String[] categories;
    LMClassifier classifier;

    void loadTrainedModel() {
        try {
            classifier = (LMClassifier) AbstractExternalizable.readObject(new File("models/sentiment_classifier.txt"));
            categories = classifier.categories();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void evaluateSentence(String sentence) {
        Classification classification = classifier.classify(sentence);
        String resultCategory = classification.bestCategory();
        System.out.println(resultCategory);
    }

//    void evaluate() throws IOException {
//        int numTests = 0;
//        int numCorrect = 0;
//        for (int i = 0; i < mCategories.length; ++i) {
//            String category = mCategories[i];
//            File file = new File(mPolarityDir,mCategories[i]);
//            File[] testFiles = file.listFiles();
//            for (int j = 0; j < testFiles.length; ++j) {
//                File testFile = testFiles[j];
//                if (!isTrainingFile(testFile)) {
//                    String review = Files.readFromFile(testFile,"ISO-8859-1");
//                    ++numTests;
//                    Classification classification = mClassifier.classify(review);
//                    String resultCategory = classification.bestCategory();
//                    if (resultCategory.equals(category))
//                        ++numCorrect;
//                }
//            }
//        }
//    }

}
