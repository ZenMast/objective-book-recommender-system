import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by shumailovr on 26.01.15.
 */

@Embeddable
public class BXRatingModelId implements Serializable {

    private int userId;
    private String isbn;

    private BXBookModel bookModel;

    public BXRatingModelId() {
    }

    public BXRatingModelId(int userId, String isbn, BXBookModel bookModel) {
        this.userId = userId;
        this.isbn = isbn;
        this.bookModel = bookModel;
    }

    @Column(name = "`User-ID`")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }



    @ManyToOne
    @JoinColumn(name = "isbn")
    public BXBookModel getBookModel() {
        return bookModel;
    }

    public void setBookModel(BXBookModel bookModel) {
        this.bookModel = bookModel;
    }


    //I know it's kinda duplication, but I need it to clean data and I don't have better way right now
    @Column(name = "ISBN")
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }


}
