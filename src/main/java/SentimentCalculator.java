import java.util.ArrayList;

/**
 * Created by shumailovr on 9.12.14.
 */
public class SentimentCalculator {

    ArrayList<String> stemmedWords;
    ArrayList<Float> valences;

    public ArrayList<String> getStemmedWords() {
        return stemmedWords;
    }

    public void setStemmedWords(ArrayList<String> stemmedWords) {
        this.stemmedWords = stemmedWords;
    }

    public ArrayList<Float> getValences() {
        return valences;
    }

    public void setValences(ArrayList<Float> valences) {
        this.valences = valences;
    }

    public float evaluate(ArrayList<String[]> tokenizedSentences) {
        int words = 0;
        float sentimentSum = 0;
        Stemmer s;

        for (String[] sentence : tokenizedSentences) {
            for (String token : sentence) {
                //Check if token is a word
                if (Character.isLetter(token.charAt(0))) {
                    //First, stem the word
                    s = new Stemmer();
                    s.stemString(token.toLowerCase());
                    //Check if such word exists in our bag of rated words, if yes - add its valence to sum
                    String stemmedToken = s.toString();
                    if (stemmedWords.contains(stemmedToken)) {
//                        System.out.println(stemmedToken);
                        Float valenceOfStemmedToken = valences.get(stemmedWords.indexOf(stemmedToken));
//                        System.out.println(valenceOfStemmedToken);
                        sentimentSum += valenceOfStemmedToken;
                        words += 1;
                    }
                }
            }
        }

        float averSentiment = sentimentSum / words;
        return averSentiment;
    }

}
