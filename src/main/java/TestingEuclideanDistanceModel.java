import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by shumailovr on 27.04.15.
 */

@Entity
@Table(name = "testing_euclidean_distances")
public class TestingEuclideanDistanceModel {

    private TestingEuclideanDistanceId testingEuclideanDistanceId;
    private double distance;

    public TestingEuclideanDistanceModel() {
    }

    public TestingEuclideanDistanceModel(TestingEuclideanDistanceId testingEuclideanDistanceId, double distance) {
        this.testingEuclideanDistanceId = testingEuclideanDistanceId;
        this.distance = distance;
    }

    @EmbeddedId
    public TestingEuclideanDistanceId getTestingEuclideanDistanceId() {
        return testingEuclideanDistanceId;
    }

    public void setTestingEuclideanDistanceId(TestingEuclideanDistanceId testingEuclideanDistanceId) {
        this.testingEuclideanDistanceId = testingEuclideanDistanceId;
    }

    @Column(name = "distance")
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}