import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by shumailovr on 02.04.15.
 */


@Entity
@Table(name = "`BX-Books`")
public class BXBookModel {

    private String isbn;
    private String title;
    private String author;
    private Set<BXRatingModel> ratings = new HashSet<BXRatingModel>();

    @Id
    @Column(name = "ISBN")
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Column(name = "`Book-Title`")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "`Book-Author`")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @OneToMany(mappedBy= "ratingModelId.bookModel")
    public Set<BXRatingModel> getRatings() {
        return ratings;
    }

    public void setRatings(Set<BXRatingModel> ratings) {
        this.ratings = ratings;
    }
}
