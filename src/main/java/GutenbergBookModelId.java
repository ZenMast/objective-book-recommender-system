import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by shumailovr on 15.04.15.
 */

@Embeddable
public class GutenbergBookModelId implements Serializable {

    private String title;
    private String author;

    public GutenbergBookModelId() {
    }

    public GutenbergBookModelId(String title, String author) {
        this.title = title;
        this.author = author;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
