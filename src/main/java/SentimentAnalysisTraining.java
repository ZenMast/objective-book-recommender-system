import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.Compilable;
import com.aliasi.util.Files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by zen on 11/16/14.
 */
public class SentimentAnalysisTraining {

    File reviewsPolarityDir;
    File sentencesPolarityDir;
    String[] categories;
    DynamicLMClassifier<NGramProcessLM> classifier;

    void setBasic() {
        reviewsPolarityDir = new File("polarity_data/reviews");
        sentencesPolarityDir = new File("polarity_data/sentences");
        categories = reviewsPolarityDir.list();
        int nGram = 8;
        classifier = DynamicLMClassifier.createNGramProcess(categories, nGram);
    }

    void trainOnSentences() {
        for (int i = 0; i < categories.length; ++i) {
            String category = categories[i];
            Classification classification = new Classification(category);
            File dir = new File(sentencesPolarityDir, categories[i]);
            File[] trainFiles = dir.listFiles();
            String trainFilePath = trainFiles[0].getPath();
            String sentencesAsString = this.readFile(trainFilePath);
            String[] sentencesAsArray = new BookAnalysator().detectSentences("models/en-sent.bin", sentencesAsString);
            for (String sentence : sentencesAsArray) {
                Classified<CharSequence> classified = new Classified<CharSequence>(sentence, classification);
                classifier.handle(classified);
            }
        }
    }

    public String readFile(String filename) {
        FileReader in = null;
        try {
            in = new FileReader(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        StringBuilder contents = new StringBuilder();
        char[] buffer = new char[4096];
        int read = 0;
        do {
            contents.append(buffer, 0, read);
            try {
                read = in.read(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (read >= 0);
        return contents.toString();
    }

    void trainOnReviews() {
        for (int i = 0; i < categories.length; ++i) {
            String category = categories[i];
            Classification classification = new Classification(category);
            File dir = new File(reviewsPolarityDir, categories[i]);
            File[] trainFiles = dir.listFiles();
            for (int j = 0; j < trainFiles.length; ++j) {
            File trainFile = trainFiles[j];
                String review = null;
                try {
                    review = Files.readFromFile(trainFile, "ISO-8859-1");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Classified<CharSequence> classified = new Classified<CharSequence>(review, classification);
                classifier.handle(classified);
            }
        }
    }

    void exportTrainedModel() {
        try {
            AbstractExternalizable.compileTo((Compilable) classifier, new File("models/sentiment_classifier.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
