# Objective book recommender system #

### What is this repository for? ###

This is repository created for code that was developed by Roman Shumailov for his bachelor's thesis at University of Tartu 2015. It includes all the necessary methods and mysqldump to repeat whole process for achieving this thesis goal results.

Objective Book Recommender System

[Download link](http://comserv.cs.ut.ee/forms/ati_report/) (Listed as Šumailov Roman    Objektiivne raamatute soovitussüsteem, thesis is in Estonian)

Abstract:
	This work’s goal was using least squares method to find linear regression model using books’ ratings as training dataset, also rate this approach effectiveness in solution of such problem. Based on calculated model it was supposed to be able to calculate differences between English books getting analysed book pairs as input. In case of every book’s content set of book describing attributes are analysed, such as text positiveness or average sentence length. Books’ ratings and open books collection were used as training dataset. As a result was found model that is able to find differences correctly in terms of training data provided, but it is not able to correctly find nearest books from human point of view due to the lack of good quality dataset. Based on model’s work correctness it was concluded that provided big enough good quality training dataset approach used in this work has potential to find model capable of finding correct nearest books in human understanding.


### How do I get set up? ###

This is IntelliJ IDEA project and can be opened as one. MySQL is used as database.

* Repo owner, email: rmshum@gmail.com